# tutorials

## working with Codeberg

* What is Codeberg, git, and a git repository and why are you using it?
* What is Woodpecker, CI/CD?
* How can I modify one of your git repositories? Use [git](git/git.md)
* How to store large files such as audio/video in a git repository? Use [git LFS](git/lfs.md)

## Jnktn.tv website

* How can I modify the pages of https://jnktn.tv? See [website documentation](https://codeberg.org/jnktn.tv/jnktn.tv/src/branch/mistress/docs/site.md)
