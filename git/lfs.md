# Git LFS

[Git Large File Storage (LFS)](https://github.com/git-lfs/git-lfs) is a Git extension for versioning large files. Git LFS saves large files such as audio/video files on a remote server while keeping pointers inside git to those files. This makes it also possible to skip automatic downloading of objects on clone or pull of a repository.

Git LFS is NOT meant to be used for file-sharing or backing up your data. Please use it responsibly!

## prerequisites

1. For using the git LFS, your server must support this feature. (Gitea has built-in LFS support which has been activated on Codeberg) Note that LFS assets are always pulled/pushed through http/https.
1. You must have git version 1.8.3.1 or newer

## installation

1. Download git LFS package. This part depends on your OS/distribution. See [git-lfs installation guide](https://github.com/git-lfs/git-lfs/wiki/Installation#debian-and-ubuntu) for some recipes. There are also a couple of [demos](#demos) at the end of this page for installing and using git LFS on various Linux distributions.
1. Activate the LSF extension for git.

    ```shell
    git lfs install
    ```

## setting up git LFS for a new repository

You need to configure the git LFS to track the files with your desired extension [Note the quotation marks around `"*.mp3"` which prevent the expansion by shell]:

```shell
git lfs track "*.mp3"
```

Git LFS configurations are saved in `.gitattributes` file. This file is created in the directory you ran the command from. For ease of maintenance, it is simpler to run the command from the root of your repository and keep all Git LFS configurations in a single file. You must commit the changes in this file to the repository.

```shell
git add .gitattributes
git commit -m "setup git-lfs to track mp3 files"
```

## adding files to a git repository where git LFS is already active

This procedure is the same as adding any other file to a git repository. If the git LFS is configured to handle the newly added file's extension, it will do it transparently. Otherwise, you have to first configure git LFS to track the files with that specific extension (see above) and then add your files to the repository.

```shell
git add new_file.mp3
git commit -m "add new_file.mp3"
```

## listing the files tracked by git LFS

```shell
git lfs ls-files
```

## cloning a git repository without downloading the LFS files

```shell
GIT_LFS_SKIP_SMUDGE=1 git clone https://codeberg.org/jnktn/XXX
```

You also have to set this environment variable if you want to checkout a different branch without downloading the LFS files:

```shell
GIT_LFS_SKIP_SMUDGE=1 git checkout new_branch 
```

## pulling a single LFS file

If you have cloned the repository without downloading the LFS files, you can pull them using `lfs pull` as follows:

```shell
git lfs pull --include="new_file.mp3"
```

## more info

- [Git LFS tutorial](https://github.com/git-lfs/git-lfs/wiki/Tutorial)

## demos

Here's a couple of examples for installing and using git LFS on various Linux distributions (the executed commands are the lines starting with `+`):

- [Almalinux 8.6](https://ci.codeberg.org/jnktn/tutorials/build/30/3)
- [Ubuntu 22.04](https://ci.codeberg.org/jnktn/tutorials/build/30/4)
- [Fedora 36](https://ci.codeberg.org/jnktn/tutorials/build/30/5)
- [Pop!_OS 22.04](https://ci.codeberg.org/jnktn/tutorials/build/30/6)
